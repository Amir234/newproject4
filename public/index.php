<?php

//use Project4\Controller\AllCategoriesController;

use Project4\Controller\AllCategoryController;
use Project4\Controller\AllPostController;
use Project4\Controller\CreateCategoryController;
use  Project4\Controller\CreatePostController;
use Project4\Controller\FindCategoryController;
use Project4\Controller\FindPostController;
use Project4\Controller\GetBySlugController;
use Project4\Controller\HomeController;
use \Project4\Controller\OpenApiController;
use Slim\Factory\AppFactory;
//use PDO;


require __DIR__ . '/../boot.php';



$container = require __DIR__ . '/../config/container.php';

AppFactory::setContainer($container);
$app = AppFactory::create();

$app->get('/', HomeController::class);
$app->get('/openapi', OpenApiController::class);
$app->get('/apidocs', fn()=> file_get_contents(__DIR__ . '/apidocs/index.html'));
$app->get('/v1/posts', new AllPostController($container));
$app->get('/v1/posts/getSlug/{slug}', new GetBySlugController($container));
$app->get('/v1/category', new AllCategoryController($container));
$app->post('/v1/create/category', new CreateCategoryController($container));
$app->post('/v1/create/post', new CreatePostController($container));
$app->get('/v1/posts/{id}', new FindPostController($container));
$app->get('/v1/categories/{id}', new FindCategoryController($container));



$app->addErrorMiddleware(true, true, true);

$app->run();

