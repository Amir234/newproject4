<?php

use DI\Container;
use Project4\Repository\PostRepositoryFactory;
use Project4\Repository\PostRepository;
use Project4\Database\PdoFactory;
use Project4\Repository\CategoryRepository;
use Project4\Repository\CategoryRepositoryFactory;

$container = new Container();

$container->set('settings', static function() {
    return [
        'app' => [
            'domain' => $_ENV['APP_URL'] ?? 'localhost',
        ],
        'db' => [
            'host' => $_ENV['DB_HOST'] ?? 'localhost',
            'dbname' => $_ENV['DB_NAME'] ?? 'test',
            'user' => $_ENV['DB_USER'] ?? 'root',
            'pass' => $_ENV['DB_PASS'] ?? 'root',
        ]
    ];
});

$container->set('db', static function ($c) {
    $object = new PdoFactory();
    return $object($c);
});
$container->set(PostRepository::class, static fn($c) => (new PostRepositoryFactory())($c));
$container->set(CategoryRepository::class, static fn($c) => (new CategoryRepositoryFactory())($c));

return $container;
