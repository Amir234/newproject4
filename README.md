# POST API

## Installation

- clone: ``
- Goto the project folder `cd Newproject4
`
- install the composer deps: `Composer install`
- Config the evnironment: `cp .env.example .evn`
- Add your credentials to the `.env file`
- Set up the DB: `php cli/create-db.php`
- Run the application(dev mode): `php -S localhost:8889 -t public`

## App Routes

- /v1/posts
- /v1/posts/getSlug/{slug}
- /v1/category
- /v1/create/category
- /v1/create/post
- /v1/posts/{id}
- /v1/categories/{id}

- API Documentation: `/apidocs`