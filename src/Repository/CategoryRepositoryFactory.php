<?php

namespace Project4\Repository;

use DI\Container;
use Project4\Repository\CategoryRepository;
use Project4\Repository\CategoryRepositryFromPdo;

class CategoryRepositoryFactory
{
    public function __invoke(Container $container): CategoryRepository
    {
        $pdo = $container->get('db');
        return new CategoryRepositoryFromPdo($pdo);
    }
}