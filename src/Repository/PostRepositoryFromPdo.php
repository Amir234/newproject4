<?php

namespace Project4\Repository;

use PDO;
use Project4\Entity\Post;
use Ramsey\Uuid\UuidInterface;

class PostRepositoryFromPdo implements PostRepository
{
    public function __construct(private PDO $pdo)
    {    
    }
    public function store(Post $post): void
    {
        $stm = $this->pdo->prepare('INSERT INTO posts VALUES(?, ?, ?, ?, ?, ?)');
        $stm->execute([
            $post->id()->toString(),
            $post->title(),
            $post->slug(),
            $post->content(),
            //$post->thumbnail(),
            $post->author(),
            $post->postedAt()?->format('Y-m-d H:i:s'),
        ]);    
    }
    /** @return Post[] */
    public function all(): array
    {
        $result = $this->pdo->query('SELECT id, title, slug, content, author, posted_at FROM posts')
            ->fetchAll(PDO::FETCH_ASSOC);
        $post = [];
        foreach ($result as $postData) {
            $post[] = Post::populate($postData);
        }
        return $post;    
    }  

    public function find(UuidInterface $id): Post
    {
        $stm = $this->pdo->prepare(<<<SQL
            SELECT id, title, slug, content, author, postedAt 
            FROM posts
            WHERE id=?
        SQL);
        $stm->execute([$id->toString()]);
        $data = $stm->fetch(PDO::FETCH_ASSOC);
        return Post::populate($data);
    }
    public function getBySlug($slug): array
    {   
        $result = $this->pdo->query('SELECT id, title, slug, content, author, posted_at FROM posts')
        ->fetchAll(PDO::FETCH_ASSOC);
        $slug = [];
        foreach ($result as $slugData) {
        $slug[] = Post::populate($slugData);
    }
    return $slug; 
    }
}

   