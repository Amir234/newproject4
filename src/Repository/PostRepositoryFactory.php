<?php

namespace Project4\Repository;

use DI\Container;
use Project4\Repository\PostRepository;
use Project4\Repository\PostRepositoryFromPdo;

class PostRepositoryFactory
{
    public function __invoke(Container $container): PostRepository
    {
       $pdo = $container->get('db');
       return new PostRepositoryFromPdo($pdo); 
    }
}