<?php

namespace Project4\Repository;

use PDO;
use Project4\Entity\Category;
use Ramsey\Uuid\UuidInterface;

class CategoryRepositoryFromPdo implements CategoryRepository
{
    public function __construct(private PDO $pdo)
    {    
    }
    public function store(Category $category): void
    {
        $stm = $this->pdo->prepare('INSERT INTO categories VALUES (?, ?, ?)');
        $stm->execute([
            $category->id()->toString(),
            $category->name(),
            $category->description()
        ]);
    }
     /** @return Category[] */
     public function all(): array
     {
         $result = $this->pdo
             ->query('SELECT id, name, description FROM categories')
             ->fetchAll(PDO::FETCH_ASSOC);
         $category = [];
         foreach ($result as $categoryData) {
            $category[] = Category::populate($categoryData);
         }   
         return $category;    
     }  
     public function find(UuidInterface $id): Category
     {
        $stm = $this->pdo->prepare(<<<SQL
            SELECT id, name, description 
            FROM categories
            WHERE id=?
        SQL);
        $stm->execute([$id->toString()]);
        $data = $stm->fetch(PDO::FETCH_ASSOC);
        return Category::populate($data);
     }
}