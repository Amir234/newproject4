<?php

namespace Project4\Repository;

use Project4\Entity\Category;
use Ramsey\Uuid\UuidInterface;

interface CategoryRepository
{
    public function store(Category $category): void;
    /** @return Category[] */
    public function all(): array;
    public function find(UuidInterface $id): Category;
}