<?php

namespace Project4\Repository;

use Project4\Entity\Post;
use Ramsey\Uuid\UuidInterface;

interface PostRepository
{
    public function store(Post $post): void;
    /** @return Post[] */
    public function all(): array;
    public function find(UuidInterface $id): Post;
    public function getBySlug($slug): array;

}