<?php

namespace Project4\validator;

class PostInputValidator
{
  
    public function validate(array $inputs): array
    {
        $errors = [];
        if (trim($inputs['title']) == '') {
            $errors [] =  'Input field can not be empty';
        }
        if (trim($inputs['content']) == '') {
            $errors [] =  'Input field can not be empty';
        }
        return $errors;
    }
}