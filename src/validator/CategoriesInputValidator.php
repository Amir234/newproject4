<?php

namespace Project4\validator;

class CategoriesInputValidator
{
  
    public function validate(array $inputs): array
    {
        $errors = [];
        if (trim($inputs['name']) == '') {
            $errors [] =  'Input field can not be empty';
        }
        if (trim($inputs['description']) == '') {
            $errors [] =  'Input field can not be empty';
        }
        return $errors;
    }
}