<?php

namespace Project4\Controller;

use OpenApi\Annotations as OA;
use Project4\Entity\Post;

/**
 * @OA\Schema(name="PostResponse")
 */
class PostResponse
{
    public function __construct(
        /** @OA\Property(property="id", type="string", example="") */
        public readonly string $id,
         /** @OA\Property(property="title", type="string", example="title") */
        public readonly string $title,
         /** @OA\Property(property="slug", type="string", example="slug") */
         public readonly string $slug,
         /** @OA\Property(property="content", type="string", example="content") */
        public readonly string $content,
      
         /** @OA\Property(property="author", type="string", example="author name") */
        public readonly string $author,
        /** @OA\Property(property="postedAt", type="string", example="2023-10-12 13:56:00") */
        public readonly string $postedAt,
    ){}
    public static function fromPost(Post $post): self
    {
        return new PostResponse(
            $post->id()->toString(),
            $post->title(),
            $post->slug(),
            $post->content(),
            //$post->thumbnail(),
            $post->author(),
            $post->postedAt()?->format('Y-m-d H:i:s'),
        );
    }
}