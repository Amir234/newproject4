<?php

namespace Project4\Controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Project4\Repository\CategoryRepository;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Ramsey\Uuid\Uuid;
use OpenApi\Annotations as OA;

class FindCategoryController
{
    private CategoryRepository $categoryRepository;
    public function __construct(Container $container)
    {   
        $this->categoryRepository = $container->get(CategoryRepository::class);  
    }
    /**
     * @OA\Get(
     *     path="/v1/categories/{id}",
     *     description="Return categories by id.",
     *     tags={"Category"},
     *     @OA\Parameter(
     *         description="ID of category to fetch",
     *         in="path",
     *         name"id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Category response",
     *         @OA\JsonContent(ref="#/components/schemas/PostResponse")
     *     )
     * )
     */
    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $category = $this->categoryRepository->find(Uuid::fromString($args['id']));
        return new JsonResponse(CategoryResponse::fromCategory($category));  
    }
}