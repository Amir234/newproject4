<?php

namespace Project4\Controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Project4\Entity\Category;
use Project4\Repository\CategoryRepository;
use Ramsey\Uuid\Uuid;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use OpenApi\Annotations as OA;

class CreateCategoryController
{
    private CategoryRepository $categoryRepository;
    public function __construct( Container $container)
    {
        $this->categoryRepository = $container->get(CategoryRepository::class);
    }
     /**
     * @OA\Get(
     *     path="/v1/create/category",
     *     description="Create Category.",
     *     tags={}"Category"},
     *     @OA\Response(
     *         response=200,
     *         description="Category response",
     *         @OA\JsonContent(
     *             type="array",
     *              @OA\Items(ref="#/components/schemas/PostResponse")
     *         )
     *     )
     * )
     */
    /**
     * @throws JsonException
     */
    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $inputs = json_decode($request->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        $category = new Category(Uuid::uuid4(), $inputs['name'], $inputs['description']);
        $this->categoryRepository->store($category);

        $output = [
            'id' => $category->id()->toString(),
            'name' => $category->name(),
            'description' => $category->description(),
        ];

        $output = new CategoryResponse(
            $category->id()->toString(),
            $category->name(),
            $category->description()       
        );

        return new JsonResponse($output);
        
    }
}