<?php

namespace Project4\Controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Project4\Entity\Post;
use Project4\Repository\PostRepository;
use Ramsey\Uuid\Uuid;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Project4\validator\PostInputValidator;
use DateTimeImmutable;
use Cocur\Slugify\Slugify;
use OpenApi\Annotations as OA;

class CreatePostController
{
    
    private PostRepository $postRepository;

    public function __construct(private Container $container)
    {
        $this->postRepository = $container->get(PostRepository::class);
    }
    /**
     * @OA\Get(
     *     path="//v1/create/post",
     *     description="Create Post.",
     *     tags={}"Post"},
     *     @OA\Response(
     *         response=200,
     *         description="Post response",
     *         @OA\JsonContent(
     *             type="array",
     *              @OA\Items(ref="#/components/schemas/PostResponse")
     *         )
     *     )
     * )
     */
    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $inputs = json_decode($request->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        $errors = (new PostInputValidator())->validate($inputs);
        if (count($errors)) {
            $output = [
                'status' => 'error',
                'messages' => $errors,
            ];
            return new JsonResponse($output, 400);
        }

       // $unique = uniqid();
       // $b64 = $inputs['thumbnail'];

        //file_put_contents('images/' . '.jpeg', base64_decode($b64));

        $slugify = new Slugify();
        $slug = $slugify->slugify($inputs['title']);

        $post = new Post(Uuid::uuid4(), $inputs['title'], $slug, $inputs['content'], $inputs['author'], new DateTimeImmutable());
        $this->postRepository->store($post);

       $output = new PostResponse(
           $post->id()->toString(),
           $post->title(),
           $post->slug(),
           $post->content(),
           //$post->thumbnail(),
           $post->author(),
           $post->postedAt()?->format('Y-m-d H:i:s'),
       );

        return new JsonResponse($output);
    }
}