<?php

namespace Project4\Controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Project4\Repository\CategoryRepository;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use OpenApi\Annotations as OA;

class AllCategoryController
{
    private CategoryRepository $categoryRepository;
    public function __construct(Container $container)
    {
        $this->categoryRepository = $container->get(CategoryRepository::class);
    }

     /**
     * @OA\Get(
     *     path="/v1/category",
     *     description="Return all categries.",
     *     tags={}"Category"},
     *     @OA\Response(
     *         response=200,
     *         description="Category response",
     *         @OA\JsonContent(
     *             type="array",
     *              @OA\Items(ref="#/components/schemas/PostResponse")
     *         )
     *     )
     * )
     */
    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $categories = $this->categoryRepository->all();
        return $this->toJson($categories);   
    }
    private function toJson(array $categories): JsonResponse
    {
        $response = [];
        foreach ($categories as $category) {
            $response[] = new CategoryResponse(
                $category->id()->toString(),
                $category->name(),
                $category->description(),
            );
        }
        return new JsonResponse($response);
    }
   
}