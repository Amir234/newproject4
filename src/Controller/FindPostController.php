<?php

namespace Project4\Controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Project4\Repository\PostRepository;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;


class FindPostController
{
    private PostRepository $postRepository;
    public function __construct(Container $container)
    {   
        $this->postRepository = $container->get(PostRepository::class);  
    }

    /**
     * @OA\Get(
     *     path="/v1/posts/{id}",
     *     description="Return posts by id.",
     *     tags={"Posts"},
     *     @OA\Parameter(
     *         description="ID of post to fetch",
     *         in="path",
     *         name"id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Posts response",
     *         @OA\JsonContent(ref="#/components/schemas/PostResponse")
     *     )
     * )
     */
    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $post = $this->postRepository->find(Uuid::fromString($args['id']));
        return new JsonResponse(PostResponse::fromPost($post));  
    }
}