<?php

namespace Project4\Controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Project4\Repository\PostRepository;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use OpenApi\Annotations as OA;

class AllPostController
{
    private PostRepository $postRepository;
    public function __construct(Container $container)
    {   
        $this->postRepository = $container->get(PostRepository::class);  
    }

    /**
     * @OA\Get(
     *     path="/v1/posts",
     *     description="Return all posts.",
     *     tags={}"Posts"},
     *     @OA\Response(
     *         response=200,
     *         description="Posts response",
     *         @OA\JsonContent(
     *             type="array",
     *              @OA\Items(ref="#/components/schemas/PostResponse")
     *         )
     *     )
     * )
     */
    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $posts = $this->postRepository->all();
        return $this->toJson($posts);  
    }
    /** @param Post[] $posts 
     * @return jsonResponse
    */
    private function toJson(array $posts): JsonResponse
    {
        $response = [];
        foreach ($posts as $post) {
            $response[] = new PostResponse(
                $post->id()->toString(),
                $post->title(),
                $post->slug(),
                $post->content(),
                $post->thumbnail(),
                $post->author(),
                $post->postedAt()?->format('Y-m-d H:i:s'),
            );
        }
        return new JsonResponse($response);
    }
}
