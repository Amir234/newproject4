<?php

namespace Project4\Controller;

use OpenApi\Annotations as OA;
use Project4\Entity\Category;

/**
 * @OA\Schema(name="CategoryResponse")
 */
class CategoryResponse
{
    public function __construct(
        /** @OA\Property(property="id", type="string", example="") */
        public readonly string $id,
         /** @OA\Property(property="name", type="string", example="name") */
        public readonly string $name,
         /** @OA\Property(property="description", type="string", example="description") */
         public readonly string $description,
    ){}
    public static function fromCategory(Category $category): self
    {
        return new CategoryResponse(
            $category->id()->toString(),
            $category->name(),
            $category->description(),
        );
    }
}