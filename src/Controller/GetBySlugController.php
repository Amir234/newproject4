<?php

namespace Project4\Controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Project4\Repository\PostRepository;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use OpenApi\Annotations as OA;

class GetBySlugController
{
    private PostRepository $postRepository;

    public function __construct(Container $container)
    {
        $this->postRepository = $container->get(PostRepository::class);       
    }
     /**
     * @OA\Get(
     *     path="/v1/posts/getSlug/{slug}",
     *     description="Return post by slug.",
     *     tags={"Post"},
     *     @OA\Parameter(
     *         description="Slug of post to fetch",
     *         in="path",
     *         name"id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Post response",
     *         @OA\JsonContent(ref="#/components/schemas/PostResponse")
     *     )
     * )
     */
    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $post = $this->postRepository->getBySlug($args['slug']);

        return $this->toJson($post);    
    }
    private function toJson(array $post): JsonResponse
    {
        $postCategories =[];
        foreach ($post as $posts) {
            $postCategories[] = $posts->toArray();
        }
        return new JsonResponse($postCategories);
    }
}