<?php 

namespace Project4\Controller;

use Assert\Assertion;
use Assert\AssertionFailedException;

class Title
{
    /**
     * @throws AssertionFailedException
     */
    public function __construct(private readonly string $title)
    {
        Assertion::minlength($this->title, 5, 'Post title should have min of 5 letters');    
    }
    public function toString(): string
    {
        return $this->title;
    }
}